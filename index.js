/**
 * nats common module
 * @module
 */
const internals = {
  maxMessageLength: 64 * 1024,
  consoleLogFn: console.log,
  consoleErrorFn: console.error,
  tags: ['error', 'info', 'debug', 'warn'],
  normalizeLength: (args) => {
    let msg = JSON.stringify(args)
    if (msg.length > internals.maxMessageLength) {
      return msg.substr(0, internals.maxMessageLength)
    }
    return args
  }
}

const NatsLogger = function (config, appId, extraTags) {
  if (!appId) {
    throw new Error('appId cant be empty!')
  }
  internals.appId = appId
  if (extraTags && Array.isArray(extraTags) && extraTags.length) {
    extraTags = extraTags.filter(el => internals.tags.includes(el))
    if (extraTags.length) {
      internals.tags = internals.tags.concat(extraTags)
    }
  }
  config.json = true
  internals.nats = require('nats').connect(config)

  internals.nats.on('error', function (e) {
    internals.consoleErrorFn('Error [' + internals.nats.options.url + ']: ' + e)
  })

  console.log = function (...args) {
    if (args[0] && internals.tags.includes(args[0])) {
      let tag = args.shift()
      args = internals.normalizeLength(args)
      if (args.length === 1) {
        args = args[0]
        if (typeof args === 'number' || typeof args === 'string') {
          args = {args}
        }
      }
      internals.nats.publish(`logs.${internals.appId}.${tag}`, args)
    } else {
      args = internals.normalizeLength(args)
      internals.nats.publish(`logs.${internals.appId}.raw`, args)
      internals.consoleLogFn.apply(this, args)
    }
  }

  console.error = function (...args) {
    args = internals.normalizeLength(args)
    internals.nats.publish(`logs.${internals.appId}.error`, args)
    internals.consoleErrorFn.apply(this, args)
  }
}

module.exports = NatsLogger
